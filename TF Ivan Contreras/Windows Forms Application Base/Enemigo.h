
using namespace System::Drawing;

class CEnemigo
{
protected:
	int x, y, dx, dy;
	int w, h, indX, indY, dirEnemigo;
public:
	CEnemigo();
	~CEnemigo();

	virtual void Pintar(Graphics ^g, Bitmap ^img) abstract;
	virtual void Mover(Graphics ^g) abstract;

	int GetX();
	int GetY();
	int GetXW();
	int GetYH();
	int GetMovimiento();
};

