#pragma once

#include "frmMenu.h"

namespace WindowsFormsApplicationBase {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private:
		Graphics ^g;
		int contIntro;
		bool imgIntro;
		bool imgInstrucciones;
		bool imgLoading;
	


	private: System::Windows::Forms::PictureBox^  pbxInstrucciones;
	private: System::Windows::Forms::PictureBox^  pbxLoading;
	private: System::Windows::Forms::Timer^  timer1;

	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			imgIntro = true;
			imgInstrucciones = false;
			imgLoading = false;
			contIntro = 0;
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pbxIntro;
	protected:

	protected:

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->pbxIntro = (gcnew System::Windows::Forms::PictureBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->pbxInstrucciones = (gcnew System::Windows::Forms::PictureBox());
			this->pbxLoading = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxIntro))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxInstrucciones))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxLoading))->BeginInit();
			this->SuspendLayout();
			// 
			// pbxIntro
			// 
			this->pbxIntro->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxIntro.Image")));
			this->pbxIntro->Location = System::Drawing::Point(29, 36);
			this->pbxIntro->Name = L"pbxIntro";
			this->pbxIntro->Size = System::Drawing::Size(100, 50);
			this->pbxIntro->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxIntro->TabIndex = 0;
			this->pbxIntro->TabStop = false;
			this->pbxIntro->Visible = false;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// pbxInstrucciones
			// 
			this->pbxInstrucciones->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxInstrucciones.Image")));
			this->pbxInstrucciones->Location = System::Drawing::Point(29, 119);
			this->pbxInstrucciones->Name = L"pbxInstrucciones";
			this->pbxInstrucciones->Size = System::Drawing::Size(100, 50);
			this->pbxInstrucciones->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxInstrucciones->TabIndex = 1;
			this->pbxInstrucciones->TabStop = false;
			this->pbxInstrucciones->Visible = false;
			// 
			// pbxLoading
			// 
			this->pbxLoading->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxLoading.Image")));
			this->pbxLoading->Location = System::Drawing::Point(29, 205);
			this->pbxLoading->Name = L"pbxLoading";
			this->pbxLoading->Size = System::Drawing::Size(100, 50);
			this->pbxLoading->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxLoading->TabIndex = 2;
			this->pbxLoading->TabStop = false;
			this->pbxLoading->Visible = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(784, 561);
			this->Controls->Add(this->pbxLoading);
			this->Controls->Add(this->pbxInstrucciones);
			this->Controls->Add(this->pbxIntro);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(800, 600);
			this->MinimumSize = System::Drawing::Size(800, 600);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"BRAID THE GAME";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::Form1_Paint);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxIntro))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxInstrucciones))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxLoading))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e)
	{
	}
	private: System::Void Form1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e)
	{
		g = this->CreateGraphics();
		g->Clear(Color::Black);
		delete g;
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e)
	{
		g = this->CreateGraphics();
		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;

		BufferedGraphicsContext ^espacioBuffer = BufferedGraphicsManager::Current;
		espacioBuffer->MaximumBuffer = System::Drawing::Size(gWidth + 1, gHeigth + 1);
		BufferedGraphics ^buffer = espacioBuffer->Allocate(g, Drawing::Rectangle(0, 0, gWidth, gHeigth));

		buffer->Graphics->Clear(Color::Black);

		if (imgIntro == true)
			buffer->Graphics->DrawImage(pbxIntro->Image, 0, 0, gWidth, gHeigth);
		if (imgInstrucciones == true)
			buffer->Graphics->DrawImage(pbxInstrucciones->Image, 0, 0, gWidth, gHeigth);
		if (imgLoading == true)
			buffer->Graphics->DrawImage(pbxLoading->Image, 0, 0, gWidth, gHeigth);

		if (imgInstrucciones == false)
			contIntro++;

		if (contIntro == 50)
		{
			imgIntro = false;
			imgInstrucciones = true;
		}

		if (imgLoading == true)
		{
			System::Drawing::Font ^porcentaje = gcnew System::Drawing::Font("Chiller", 40, FontStyle::Bold);
			buffer->Graphics->DrawString(System::Convert::ToString(contIntro - 50) + "%", porcentaje, Brushes::Cyan, gWidth / 4 - 100, gHeigth - (gHeigth / 5));
			buffer->Graphics->DrawString(System::Convert::ToString(contIntro - 50) + "%", porcentaje, Brushes::Cyan, gWidth - 180, gHeigth - (gHeigth / 5));
			delete porcentaje;
		}

		buffer->Render(g);

		delete buffer;
		delete espacioBuffer;
		delete g;

		if (contIntro == 55)
		//if (contIntro == 150)
		{
			contIntro = 0;
			imgLoading = false;
			this->timer1->Enabled = false;

			this->Hide();
			frmMenu ^f = gcnew frmMenu();
			//f->ptrFuncion = CerrarFormularioPrincipal;
			f->Show();
		}
	}
	private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (e->KeyCode == Keys::Space && imgInstrucciones == true)
		{
			imgInstrucciones = false;
			imgLoading = true;
		}
	}
};
}

