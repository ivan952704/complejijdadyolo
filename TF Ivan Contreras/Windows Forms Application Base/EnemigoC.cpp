
#include "stdafx.h"
#include <stdio.h>

CEnemigoC::CEnemigoC(int x, int y, int dir) : CEnemigo()
{
	this->x = x;
	this->y = y;
	dirEnemigo = dir;
	NroMisiles = dx = 0;
	dy = 5;
	cont_agregar = indX = indY = 0;
	Arreglo_Misiles = NULL;
}

CEnemigoC::~CEnemigoC()
{
	for (int i = 0; i < NroMisiles; i++)
		delete Arreglo_Misiles[i];

	if (Arreglo_Misiles != NULL)
		delete []Arreglo_Misiles;
}

void CEnemigoC::Pintar(Graphics ^g, Bitmap ^img)
{
	w = img->Width / 6;
	h = img->Height;

	Rectangle porcion_imagen = Rectangle(w * indX, h * indY, w, h);
	w = 80;
	h = 120;
	Rectangle zoom = Rectangle(x, y, w, h);
	g->DrawImage(img, zoom, porcion_imagen, GraphicsUnit::Pixel);


	indX++;
	if (indX >= 6)
		indX = 0;
}

void CEnemigoC::Mover(Graphics ^g)
{
	y += dy;
	if (y + dy<0 || y + dy + h>g->VisibleClipBounds.Height)
		dy *= -1;
}

void CEnemigoC::Pintar_Balas(Graphics ^g, Bitmap ^img)
{
	for (int i = 0; i < NroMisiles; i++)
		Arreglo_Misiles[i]->Pintar(g, img);
}

void CEnemigoC::Mover_Balas(Graphics ^g)
{
	for (int i = 0; i < NroMisiles; i++)
		Arreglo_Misiles[i]->Mover(g);
}

void CEnemigoC::Agregar_Bala()
{
	cont_agregar++;
	if (cont_agregar >= 60)
	{
		CMisil **Temp = new CMisil*[NroMisiles + 1];

		for (int i = 0; i < NroMisiles; i++)
			Temp[i] = Arreglo_Misiles[i];

		Temp[NroMisiles] = new CMisil(x - 8 + w / 2, y + 4, dirEnemigo);

		NroMisiles += 1;
		if (Arreglo_Misiles != NULL)
			delete[]Arreglo_Misiles;

		Arreglo_Misiles = Temp;
		cont_agregar = 0;
	}
}

void CEnemigoC::Eliminar_Bala()
{
	if (NroMisiles > 0 && Arreglo_Misiles[0]->GetDX() == 0)
	{
		int cont = 0;
		for (int i = 0; i < NroMisiles; i++)
			if (Arreglo_Misiles[i]->GetDX() == 0)
				cont++;

		CMisil **Temp = new CMisil*[NroMisiles - cont];

		int k = 0;
		for (int i = 0; i < NroMisiles; i++)
			if (Arreglo_Misiles[i]->GetDX() != 0)
			{
				Temp[k] = Arreglo_Misiles[i];
				k++;
			}

		NroMisiles -= cont;
		if (Arreglo_Misiles != NULL)
			delete[]Arreglo_Misiles;

		Arreglo_Misiles = Temp;
		if (NroMisiles == 0)
			Arreglo_Misiles = NULL;
	}
}

int CEnemigoC::GetNumeroMisiles()
{
	return NroMisiles;
}

CMisil *CEnemigoC::GetMisil(int indice)
{
	return Arreglo_Misiles[indice];
}
