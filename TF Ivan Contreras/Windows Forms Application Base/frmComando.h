#pragma once

namespace WindowsFormsApplicationBase {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for frmComando
	/// </summary>
	public ref class frmComando : public System::Windows::Forms::Form
	{
	public:
		frmComando(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~frmComando()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pbxFondo;

	private: System::Windows::Forms::Button^  button2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmComando::typeid));
			this->pbxFondo = (gcnew System::Windows::Forms::PictureBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->BeginInit();
			this->SuspendLayout();
			// 
			// pbxFondo
			// 
			this->pbxFondo->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxFondo.Image")));
			this->pbxFondo->Location = System::Drawing::Point(29, 27);
			this->pbxFondo->Name = L"pbxFondo";
			this->pbxFondo->Size = System::Drawing::Size(86, 38);
			this->pbxFondo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxFondo->TabIndex = 0;
			this->pbxFondo->TabStop = false;
			this->pbxFondo->Visible = false;
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->Location = System::Drawing::Point(128, 329);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(119, 31);
			this->button2->TabIndex = 9;
			this->button2->Text = L"Aceptar";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &frmComando::button2_Click);
			// 
			// frmComando
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(374, 371);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->pbxFondo);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(390, 410);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(390, 410);
			this->Name = L"frmComando";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"COMANDOS";
			this->Load += gcnew System::EventHandler(this, &frmComando::frmComando_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &frmComando::frmComando_Paint);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void frmComando_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		Graphics ^g = this->CreateGraphics();
		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;
		g->DrawImage(pbxFondo->Image, 0, 0, gWidth, gHeigth);
		delete g;
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	}
	private: System::Void frmComando_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
};
}
