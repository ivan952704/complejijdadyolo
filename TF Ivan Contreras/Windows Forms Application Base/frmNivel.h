#pragma once

#include "frmJuego.h"

namespace WindowsFormsApplicationBase {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for frmNivel
	/// </summary>
	public ref class frmNivel : public System::Windows::Forms::Form
	{
	public:
		CJuego *objJuego;
		bool autoPlay;
	public:
		frmNivel(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			objJuego = NULL;
			autoPlay = false;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~frmNivel()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  btnNivel1;
	private: System::Windows::Forms::PictureBox^  pbx3;
	protected:

	private: System::Windows::Forms::PictureBox^  pbx1;

	private: System::Windows::Forms::Button^  btnNivel2;
	private: System::Windows::Forms::PictureBox^  pbx2;

	private: System::Windows::Forms::Button^  btnNivel3;
	private: System::Windows::Forms::PictureBox^  pbxFondo;
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmNivel::typeid));
			this->btnNivel1 = (gcnew System::Windows::Forms::Button());
			this->pbx3 = (gcnew System::Windows::Forms::PictureBox());
			this->pbx1 = (gcnew System::Windows::Forms::PictureBox());
			this->btnNivel2 = (gcnew System::Windows::Forms::Button());
			this->pbx2 = (gcnew System::Windows::Forms::PictureBox());
			this->btnNivel3 = (gcnew System::Windows::Forms::Button());
			this->pbxFondo = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->BeginInit();
			this->SuspendLayout();
			// 
			// btnNivel1
			// 
			this->btnNivel1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->btnNivel1->Location = System::Drawing::Point(299, 177);
			this->btnNivel1->Name = L"btnNivel1";
			this->btnNivel1->Size = System::Drawing::Size(119, 42);
			this->btnNivel1->TabIndex = 0;
			this->btnNivel1->Text = L"NIVEL 1";
			this->btnNivel1->UseVisualStyleBackColor = true;
			this->btnNivel1->Click += gcnew System::EventHandler(this, &frmNivel::btnNivel1_Click);
			// 
			// pbx3
			// 
			this->pbx3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbx3.Image")));
			this->pbx3->Location = System::Drawing::Point(502, 131);
			this->pbx3->Name = L"pbx3";
			this->pbx3->Size = System::Drawing::Size(198, 88);
			this->pbx3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbx3->TabIndex = 1;
			this->pbx3->TabStop = false;
			this->pbx3->Visible = false;
			// 
			// pbx1
			// 
			this->pbx1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbx1.Image")));
			this->pbx1->Location = System::Drawing::Point(260, 64);
			this->pbx1->Name = L"pbx1";
			this->pbx1->Size = System::Drawing::Size(198, 88);
			this->pbx1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbx1->TabIndex = 3;
			this->pbx1->TabStop = false;
			// 
			// btnNivel2
			// 
			this->btnNivel2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->btnNivel2->Location = System::Drawing::Point(65, 244);
			this->btnNivel2->Name = L"btnNivel2";
			this->btnNivel2->Size = System::Drawing::Size(119, 42);
			this->btnNivel2->TabIndex = 2;
			this->btnNivel2->Text = L"NIVEL 2";
			this->btnNivel2->UseVisualStyleBackColor = true;
			this->btnNivel2->Visible = false;
			this->btnNivel2->Click += gcnew System::EventHandler(this, &frmNivel::btnNivel2_Click);
			// 
			// pbx2
			// 
			this->pbx2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbx2.Image")));
			this->pbx2->Location = System::Drawing::Point(25, 131);
			this->pbx2->Name = L"pbx2";
			this->pbx2->Size = System::Drawing::Size(198, 88);
			this->pbx2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbx2->TabIndex = 5;
			this->pbx2->TabStop = false;
			this->pbx2->Visible = false;
			// 
			// btnNivel3
			// 
			this->btnNivel3->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->btnNivel3->Location = System::Drawing::Point(548, 244);
			this->btnNivel3->Name = L"btnNivel3";
			this->btnNivel3->Size = System::Drawing::Size(119, 42);
			this->btnNivel3->TabIndex = 4;
			this->btnNivel3->Text = L"NIVEL 3";
			this->btnNivel3->UseVisualStyleBackColor = true;
			this->btnNivel3->Visible = false;
			this->btnNivel3->Click += gcnew System::EventHandler(this, &frmNivel::btnNivel3_Click);
			// 
			// pbxFondo
			// 
			this->pbxFondo->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxFondo.Image")));
			this->pbxFondo->Location = System::Drawing::Point(25, 64);
			this->pbxFondo->Name = L"pbxFondo";
			this->pbxFondo->Size = System::Drawing::Size(100, 50);
			this->pbxFondo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxFondo->TabIndex = 6;
			this->pbxFondo->TabStop = false;
			this->pbxFondo->Visible = false;
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button1->Location = System::Drawing::Point(299, 244);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(119, 42);
			this->button1->TabIndex = 7;
			this->button1->Text = L"Regresar al Menu";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &frmNivel::button1_Click);
			// 
			// frmNivel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 14);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(746, 316);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pbxFondo);
			this->Controls->Add(this->pbx2);
			this->Controls->Add(this->btnNivel3);
			this->Controls->Add(this->pbx1);
			this->Controls->Add(this->btnNivel2);
			this->Controls->Add(this->pbx3);
			this->Controls->Add(this->btnNivel1);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(762, 355);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(762, 355);
			this->Name = L"frmNivel";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"NIVEL";
			this->Load += gcnew System::EventHandler(this, &frmNivel::frmNivel_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &frmNivel::frmNivel_Paint);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void btnNivel1_Click(System::Object^  sender, System::EventArgs^  e) {
		frmJuego ^f = gcnew frmJuego();
		f->objJuego = this->objJuego;
		f->autoPlay = this->autoPlay;
		f->objJuego->SetNivelActual(1);
		f->objJuego->SetPersonajeY(260);
		this->Close();
		f->ShowDialog();
	}
	private: System::Void btnNivel2_Click(System::Object^  sender, System::EventArgs^  e) {
		frmJuego ^f = gcnew frmJuego();
		f->objJuego = this->objJuego;
		f->autoPlay = this->autoPlay;
		f->objJuego->SetNivelActual(2);
		f->objJuego->SetPersonajeY(320);
		this->Close();
		f->ShowDialog();
	}
	private: System::Void frmNivel_Load(System::Object^  sender, System::EventArgs^  e) {
		if (objJuego->GetNivelDesbloqueado() > 1)
		{
			pbx2->Visible = true;
			btnNivel2->Visible = true;
		}
		if (objJuego->GetNivelDesbloqueado() == 3)
		{
			pbx3->Visible = true;
			btnNivel3->Visible = true;
		}
	}
	private: System::Void frmNivel_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		Graphics ^g = this->CreateGraphics();
		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;
		g->DrawImage(pbxFondo->Image, 0, 0, gWidth, gHeigth);
		delete g;
	}
	private: System::Void btnNivel3_Click(System::Object^  sender, System::EventArgs^  e) {
		frmJuego ^f = gcnew frmJuego();
		f->objJuego = this->objJuego;
		f->autoPlay = this->autoPlay;
		f->objJuego->SetNivelActual(3);
		f->objJuego->SetPersonajeY(235);
		this->Close();
		f->ShowDialog();
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	};
}
