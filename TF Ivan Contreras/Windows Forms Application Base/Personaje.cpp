
#include "stdafx.h"
#include "Personaje.h"

using namespace System::Drawing;

CPersonaje::CPersonaje()
{
	x = 450;
	dx = dy = indX = indY = 0;
	dir = Direccion::Ninguno;
	contSalto = hmax = 0;
	saltando = false;
}

CPersonaje::~CPersonaje()
{
}

void CPersonaje::Pintar(Graphics ^g, Bitmap ^img)
{
	w = img->Width / 26;
	h = img->Height / 5;

	Rectangle porcion_imagen = Rectangle(w * indX, h * indY, w, h);
	w = 60;
	h = 80;
	Rectangle zoom = Rectangle(x, y, w, h);
	g->DrawImage(img, zoom, porcion_imagen, GraphicsUnit::Pixel);

	if (dir == Direccion::Derecha)
	{
		indX++;
		if (indX == 26)
		{
			dir = Direccion::Ninguno;
			indY = 0;
			indX = 0;
			dx = 0;
		}
	}
	if (dir == Direccion::Izquierda)
	{
		indX--;
		if (indX == -1)
		{
			dir = Direccion::Ninguno;
			indY = 0;
			indX = 1;
			dx = 0;
		}
	}
	if (dir == Direccion::SaltoDerecha)
	{
		if (indX != 10)
			indX++;

		if (indX == 10)
			contSalto++;

		if (contSalto == 1)
			dy = 10;

		if (contSalto == 11)
		{
			dir = Direccion::Ninguno;
			indY = 0;
			indX = 0;
			dx = dy = 0;
			contSalto = 0;
		}
	}
	if (dir == Direccion::SaltoIzquierda)
	{
		if (indX != 0)
			indX--;

		if (indX == 0)
			contSalto++;

		if (contSalto == 1)
			dy = 10;

		if (contSalto == 11)
		{
			dir = Direccion::Ninguno;
			indY = 0;
			indX = 1;
			dx = dy = 0;
			contSalto = 0;
		}
	}
}

void CPersonaje::CambioDireccion(Direccion dir)
{
	if (dir == Direccion::Izquierda)
	{
		indX = 25;
		indY = 2;
		dx = -5;
	}
	if (dir == Direccion::Derecha)
	{
		indX = 0;
		indY = 1;
		dx = 5;
	}
	if (dir == Direccion::Ninguno)
	{
		if (indY == 1)//der
		{
			indY = 0;
			indX = 0;
		}
		if (indY == 2)
		{
			indY = 0;
			indX = 1;
		}
		dx = 0;
	}
	if (dir == Direccion::SaltoDerecha)
	{
		dx = 0;
		dy = -10;
		indX = 0;
		indY = 3;
	}
	if (dir == Direccion::SaltoIzquierda)
	{
		dx = 0;
		dy = -10;
		indX = 10;
		indY = 4;
	}
	this->dir = dir;
}

void CPersonaje::Mover(Graphics ^g)
{
	if (dir != Direccion::Ninguno)
	{
		if (x + dx < 0 || x + dx + w > g->VisibleClipBounds.Width)
			dx = 0;
		x += dx;
		y += dy;
	}
}

void CPersonaje::Inicializar_Salto()
{
	if (indX == 0)//der
	{
		if (x + 3 * dx > 500)
			return;

		dx = 5;
		indX = 0;
		indY = 3;
	}
	else
	{
		if (x + 3 * dx < 0)
			return;

		dx = -5;
		indX = 10;
		indY = 4;
	}

	dy = 15;
	hmax = 50;
	saltando = true;
}

void CPersonaje::Saltar(int posY)
{
	if (saltando == true)
	{
		y -= dy;
		x += dx;

		if (y <= hmax)
			dy *= -1;

		if (y >= posY)
		{
			dy = dx = 0;
			saltando = false;
			if (indX == 0)//derecha
			{
				indX = 0;
				indY = 0;
			}
			else
			{
				indX = 1;
				indY = 0;
			}

			if (x < 0)
				x = 940 - w;

			if (x + w > 950)
				x = 0;

			return;
		}
		dy -= 0.8;
	}
}

float CPersonaje::GetX()
{
	return x;
}

void CPersonaje::SetX(float x)
{
	this->x = x;
}

float CPersonaje::GetY()
{
	return y;
}

void CPersonaje::SetY(float y)
{
	this->y = y;
}

int CPersonaje::GetXW()
{
	return x + w;
}

int CPersonaje::GetYH()
{
	return y + h;
}

Direccion CPersonaje::GetDireccion()
{
	return dir;
}

int CPersonaje::GetIndiceX()
{
	return indX;
}

void CPersonaje::SetIndX(int indX)
{
	this->indX = indX;
}

int CPersonaje::GetIndiceY()
{
	return indY;
}

void CPersonaje::SetIndY(int indY)
{
	this->indY = indY;
}

int CPersonaje::Get_Contador()
{
	return contSalto;
}

bool CPersonaje::Get_Saltando()
{
	return saltando;
}
