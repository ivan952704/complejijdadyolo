
#include "stdafx.h"
//#include "EnemigoA.h"

CEnemigoA::CEnemigoA(int x, int y, int dir) : CEnemigo()
{
	this->x = x;
	this->y = y;
	dirEnemigo = dir;
	dy = 0;

	if (dir == 0)
	{
		indX = indY = 0;
		dx = 3;
	}
	else
	{
		indX = 15;
		indY = 1;
		dx = -3;
	}
}

CEnemigoA::~CEnemigoA()
{
}

void CEnemigoA::Pintar(Graphics ^g, Bitmap ^img)
{
	w = img->Width / 16;
	h = img->Height / 2;

	Rectangle porcion_imagen = Rectangle(w * indX, h * indY, w, h);
	w = 60;
	h = 80;
	Rectangle zoom = Rectangle(x, y, w, h);
	g->DrawImage(img, zoom, porcion_imagen, GraphicsUnit::Pixel);

	if (dirEnemigo == 0)
	{
		indX++;
		if (indX == 16)
			indX = 0;
	}
	else
	{
		indX--;
		if (indX == -1)
			indX = 15;
	}
}

void CEnemigoA::Mover(Graphics ^g)
{
	x += dx;
}
