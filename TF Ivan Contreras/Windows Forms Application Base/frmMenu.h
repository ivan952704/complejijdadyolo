#pragma once

#include "frmNivel.h"
#include "frmInstrucciones.h"
#using <system.dll>
using namespace System::Diagnostics;
using namespace System::Collections::Generic;

namespace WindowsFormsApplicationBase {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for frmMenu
	/// </summary>
	public ref class frmMenu : public System::Windows::Forms::Form
	{
	private:
		CJuego *objJuego;
		int cont_truco;
		Process^ myProcess = nullptr;
		bool autoPlay = false;
	private: System::Windows::Forms::ToolStripMenuItem^  nuevaPartidaToolStripMenuItem;
	private: System::Windows::Forms::Label^  lblPuntos;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::Timer^  timer1;


	public:
		frmMenu(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			objJuego = new CJuego();
			cont_truco = 0;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~frmMenu()
		{
			if (components)
			{
				delete components;
			}
			delete objJuego;
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  jugarToolStripMenuItem;

	private: System::Windows::Forms::ToolStripMenuItem^  salirToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  salirToolStripMenuItem1;


	private: System::Windows::Forms::PictureBox^  pbxFondo;




	private: System::Windows::Forms::ToolStripMenuItem^  instruccionesToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  guardarPartidaToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  cargarPartidaToolStripMenuItem;

	private: System::ComponentModel::IContainer^  components;


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmMenu::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->jugarToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->instruccionesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->guardarPartidaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cargarPartidaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->nuevaPartidaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->salirToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->salirToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pbxFondo = (gcnew System::Windows::Forms::PictureBox());
			this->lblPuntos = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->BackColor = System::Drawing::Color::White;
			this->menuStrip1->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"menuStrip1.BackgroundImage")));
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
				this->jugarToolStripMenuItem,
					this->instruccionesToolStripMenuItem, this->salirToolStripMenuItem, this->toolStripMenuItem1, this->salirToolStripMenuItem1
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(804, 27);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			this->menuStrip1->ItemClicked += gcnew System::Windows::Forms::ToolStripItemClickedEventHandler(this, &frmMenu::menuStrip1_ItemClicked);
			// 
			// jugarToolStripMenuItem
			// 
			this->jugarToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->jugarToolStripMenuItem->ForeColor = System::Drawing::Color::White;
			this->jugarToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"jugarToolStripMenuItem.Image")));
			this->jugarToolStripMenuItem->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->jugarToolStripMenuItem->Name = L"jugarToolStripMenuItem";
			this->jugarToolStripMenuItem->Size = System::Drawing::Size(81, 23);
			this->jugarToolStripMenuItem->Text = L"Jugar";
			this->jugarToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMenu::jugarToolStripMenuItem_Click);
			// 
			// instruccionesToolStripMenuItem
			// 
			this->instruccionesToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->guardarPartidaToolStripMenuItem,
					this->cargarPartidaToolStripMenuItem, this->nuevaPartidaToolStripMenuItem
			});
			this->instruccionesToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->instruccionesToolStripMenuItem->ForeColor = System::Drawing::Color::White;
			this->instruccionesToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"instruccionesToolStripMenuItem.Image")));
			this->instruccionesToolStripMenuItem->Name = L"instruccionesToolStripMenuItem";
			this->instruccionesToolStripMenuItem->Size = System::Drawing::Size(91, 23);
			this->instruccionesToolStripMenuItem->Text = L"Partida";
			// 
			// guardarPartidaToolStripMenuItem
			// 
			this->guardarPartidaToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"guardarPartidaToolStripMenuItem.Image")));
			this->guardarPartidaToolStripMenuItem->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->guardarPartidaToolStripMenuItem->Name = L"guardarPartidaToolStripMenuItem";
			this->guardarPartidaToolStripMenuItem->Size = System::Drawing::Size(198, 24);
			this->guardarPartidaToolStripMenuItem->Text = L"Guardar Partida";
			this->guardarPartidaToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMenu::guardarPartidaToolStripMenuItem_Click);
			// 
			// cargarPartidaToolStripMenuItem
			// 
			this->cargarPartidaToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"cargarPartidaToolStripMenuItem.Image")));
			this->cargarPartidaToolStripMenuItem->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->cargarPartidaToolStripMenuItem->Name = L"cargarPartidaToolStripMenuItem";
			this->cargarPartidaToolStripMenuItem->Size = System::Drawing::Size(198, 24);
			this->cargarPartidaToolStripMenuItem->Text = L"Cargar Partida";
			this->cargarPartidaToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMenu::cargarPartidaToolStripMenuItem_Click);
			// 
			// nuevaPartidaToolStripMenuItem
			// 
			this->nuevaPartidaToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"nuevaPartidaToolStripMenuItem.Image")));
			this->nuevaPartidaToolStripMenuItem->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->nuevaPartidaToolStripMenuItem->Name = L"nuevaPartidaToolStripMenuItem";
			this->nuevaPartidaToolStripMenuItem->Size = System::Drawing::Size(198, 24);
			this->nuevaPartidaToolStripMenuItem->Text = L"Nueva Partida";
			this->nuevaPartidaToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMenu::nuevaPartidaToolStripMenuItem_Click);
			// 
			// salirToolStripMenuItem
			// 
			this->salirToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->salirToolStripMenuItem->ForeColor = System::Drawing::Color::White;
			this->salirToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"salirToolStripMenuItem.Image")));
			this->salirToolStripMenuItem->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->salirToolStripMenuItem->Name = L"salirToolStripMenuItem";
			this->salirToolStripMenuItem->Size = System::Drawing::Size(141, 23);
			this->salirToolStripMenuItem->Text = L"Instrucciones";
			this->salirToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMenu::salirToolStripMenuItem_Click);
			// 
			// salirToolStripMenuItem1
			// 
			this->salirToolStripMenuItem1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->salirToolStripMenuItem1->ForeColor = System::Drawing::Color::White;
			this->salirToolStripMenuItem1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"salirToolStripMenuItem1.Image")));
			this->salirToolStripMenuItem1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->salirToolStripMenuItem1->Name = L"salirToolStripMenuItem1";
			this->salirToolStripMenuItem1->Size = System::Drawing::Size(71, 23);
			this->salirToolStripMenuItem1->Text = L"Salir";
			this->salirToolStripMenuItem1->Click += gcnew System::EventHandler(this, &frmMenu::salirToolStripMenuItem1_Click);
			// 
			// pbxFondo
			// 
			this->pbxFondo->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxFondo.Image")));
			this->pbxFondo->Location = System::Drawing::Point(12, 188);
			this->pbxFondo->Name = L"pbxFondo";
			this->pbxFondo->Size = System::Drawing::Size(100, 50);
			this->pbxFondo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxFondo->TabIndex = 2;
			this->pbxFondo->TabStop = false;
			this->pbxFondo->Visible = false;
			// 
			// lblPuntos
			// 
			this->lblPuntos->AutoSize = true;
			this->lblPuntos->BackColor = System::Drawing::Color::Transparent;
			this->lblPuntos->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->lblPuntos->Font = (gcnew System::Drawing::Font(L"Comic Sans MS", 20.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lblPuntos->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"lblPuntos.Image")));
			this->lblPuntos->Location = System::Drawing::Point(460, 413);
			this->lblPuntos->Name = L"lblPuntos";
			this->lblPuntos->Size = System::Drawing::Size(131, 40);
			this->lblPuntos->TabIndex = 4;
			this->lblPuntos->Text = L"lblPuntos";
			this->lblPuntos->Click += gcnew System::EventHandler(this, &frmMenu::label2_Click);
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &frmMenu::timer1_Tick);
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->toolStripMenuItem1->ForeColor = System::Drawing::Color::White;
			this->toolStripMenuItem1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripMenuItem1.Image")));
			this->toolStripMenuItem1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(106, 23);
			this->toolStripMenuItem1->Text = L"AutoPlay";
			this->toolStripMenuItem1->Click += gcnew System::EventHandler(this, &frmMenu::toolStripMenuItem1_Click);
			// 
			// frmMenu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
			this->ClientSize = System::Drawing::Size(804, 462);
			this->Controls->Add(this->lblPuntos);
			this->Controls->Add(this->pbxFondo);
			this->Controls->Add(this->menuStrip1);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Location = System::Drawing::Point(820, 500);
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(820, 500);
			this->MinimumSize = System::Drawing::Size(820, 500);
			this->Name = L"frmMenu";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"MENU";
			this->Load += gcnew System::EventHandler(this, &frmMenu::frmMenu_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &frmMenu::frmMenu_Paint);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &frmMenu::frmMenu_KeyDown);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxFondo))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void frmMenu_Load(System::Object^  sender, System::EventArgs^  e) {
		FILE *file = NULL;
		char *fileName = "MisNiveles.txt";
		file = fopen(fileName, "wb+");
		if (file != NULL)
		{
			objJuego->Crear_Niveles(file);
			fclose(file);
			file = NULL;
		}
		else
		{
			MessageBox::Show("Error al crear los niveles",
				"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Warning);
			this->Close();
		}
		file = fopen(fileName, "rb");
		if (file != NULL)
		{
			objJuego->Cargar_Niveles(file);
			fclose(file);
			file = NULL;
		}
		else
		{
			MessageBox::Show("Error al cargar los niveles",
				"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Warning);
			this->Close();
		}
		myProcess = Process::GetCurrentProcess();
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void salirToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		frmInstrucciones ^f = gcnew frmInstrucciones();
		f->ShowDialog();
	}
	private: System::Void salirToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (MessageBox::Show("�Desea salir del juego?",
			"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Question) != System::Windows::Forms::DialogResult::Yes)
			return;

		this->Close();
		myProcess->Kill();
	}
	private: System::Void jugarToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		frmNivel ^f = gcnew frmNivel();
		f->objJuego = this->objJuego;
		f->autoPlay = this->autoPlay;
		f->ShowDialog();
	}
	private: System::Void guardarPartidaToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (MessageBox::Show("�Est� seguro de guardar la partida actual?",
			"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Question) != System::Windows::Forms::DialogResult::Yes)
			return;

		FILE *file = NULL;
		char *fileName = "MiPartida.txt";
		file = fopen(fileName, "wb+");
		if (file != NULL)
		{
			objJuego->Guardar_Partida(file);
			fclose(file);
			file = NULL;
		}
		else
		{
			MessageBox::Show("Error al guardar la partida",
				"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Warning);
			return;
		}
		MessageBox::Show("Se guard� la partida satisfactoriamente",
			"Braid The Game", MessageBoxButtons::OK, MessageBoxIcon::Information);
	}
	private: System::Void cargarPartidaToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (MessageBox::Show("�Est� seguro de cargar una partida y dejar la partida actual?",
			"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Question) != System::Windows::Forms::DialogResult::Yes)
			return;

		FILE *file = NULL;
		char *fileName = "MiPartida.txt";
		file = fopen(fileName, "rb");
		if (file != NULL)
		{
			objJuego->Cargar_Partida(file);
			fclose(file);
			file = NULL;
		}
		else
		{
			MessageBox::Show("Error al cargar la partida",
				"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Warning);
			return;
		}

		MessageBox::Show("Se carg� la partida satisfactoriamente",
			"Braid The Game", MessageBoxButtons::OK, MessageBoxIcon::Information);
	}
	private: System::Void frmMenu_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		Graphics ^g = this->CreateGraphics();
		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;
		g->DrawImage(pbxFondo->Image, 0, 0, gWidth, gHeigth);
		delete g;
	}

	private: System::Void frmMenu_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
		if (e->KeyCode == Keys::U)
		{
			if (cont_truco == 0)
				cont_truco++;
			else
				cont_truco = 0;
		}
		if (e->KeyCode == Keys::P)
		{
			if (cont_truco == 1)
				cont_truco++;
			else
				cont_truco = 0;
		}
		if (e->KeyCode == Keys::C)
		{
			if (cont_truco == 2)
				objJuego->SetNivelDesbloqueado(3);
			cont_truco = 0;
		}
	}
	private: System::Void nuevaPartidaToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (MessageBox::Show("�Est� seguro de crear una nueva partida y dejar la partida actual?",
			"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Question) != System::Windows::Forms::DialogResult::Yes)
			return;

		objJuego->SetNivelDesbloqueado(1);
		objJuego->SetPuntosGanados(0);

		MessageBox::Show("Se cre� una nueva partida satisfactoriamente",
			"Braid The Game", MessageBoxButtons::OK, MessageBoxIcon::Information);
	}
	private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		lblPuntos->Text = "Puntos Ganados: " + System::Convert::ToString(objJuego->GetPuntosGanados());
	}
	private: System::Void menuStrip1_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^  e) {
	}
	private: System::Void toolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {

		String ^estado = autoPlay ? "desactivar" : "activar";

		if (MessageBox::Show("�Desea " + estado + " el autoplay?",
			"Braid The Game", MessageBoxButtons::YesNo, MessageBoxIcon::Question) != System::Windows::Forms::DialogResult::Yes)
			return;

		autoPlay = !autoPlay;
	}
	};
}
