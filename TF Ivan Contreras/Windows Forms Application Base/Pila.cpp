
#include "stdafx.h"
#include "Pila.h"
#include <stdio.h>

CPila::CPila()
{
	first = NULL;
	nele = 0;
}

CPila::~CPila()
{
	Clear();
}

void CPila::Push(float x, float y, int indX, int indY)
{
	CNodo *nuevo = new CNodo();
	nuevo->SetX(x);
	nuevo->SetY(y);
	nuevo->SetIndX(indX);
	nuevo->SetIndY(indY);
	nuevo->SetNext(first);

	first = nuevo;
	nele++;
}

bool CPila::Pop()
{
	if (nele == 0)
		return false;

	CNodo *aux = first;
	first = aux->GetNext();

	nele--;
	delete aux;
	aux = NULL;
	return true;
}

CNodo *CPila::Top()
{
	if (nele > 0)
		return first;
}

bool CPila::Empty()
{
	if (nele > 0)
		return false;
	return true;
}

int CPila::Size()
{
	return nele;
}

void CPila::Clear()
{
	while (Pop());
}
