
#include "stdafx.h"

CMisil::CMisil(int x, int y, int dir)
{
	this->x = x;
	this->y = y;
	ind = 0;
	System::Random r;
	dx = dir == 0 ? -5 : 5;
}

CMisil::~CMisil()
{
}

void CMisil::Pintar(Graphics ^g, Bitmap ^img)
{
	w = img->Width / 4;
	h = img->Height;

	Rectangle porcion_imagen = Rectangle(ind * w, 0, w, h);

	g->DrawImage(img, x, y, porcion_imagen, GraphicsUnit::Pixel);

	ind++;
	if (ind >= 4)
		ind = 0;
}

void CMisil::Mover(Graphics ^g)
{
	if (x + dx < 0 || x + dx + w > g->VisibleClipBounds.Width)
		dx = 0;
	x += dx;
}

int CMisil::GetX()
{
	return x;
}

int CMisil::GetY()
{
	return y;
}

int CMisil::GetXW()
{
	return x + w;
}

int CMisil::GetYH()
{
	return y + h;
}

int CMisil::GetDX()
{
	return dx;
}

void CMisil::SetDX(int dx)
{
	this->dx = dx;
}
