
using namespace System::Drawing;

public enum Direccion { SaltoIzquierda, SaltoDerecha, Derecha, Izquierda, Ninguno };
class CPersonaje
{
private:
	float x, y, dx, dy;
	int w, h, indX, indY;
	int contSalto, hmax;
	bool saltando;
	Direccion dir;
public:
	CPersonaje();
	~CPersonaje();

	void Pintar(Graphics ^g, Bitmap ^img);
	void Mover(Graphics ^g);
	void CambioDireccion(Direccion dir);
	void Saltar(int posY);
	void Inicializar_Salto();

	float GetX();
	void SetX(float x);
	float GetY();
	void SetY(float y);
	int GetXW();
	int GetYH();
	int GetIndiceX();
	void SetIndX(int indX);
	int GetIndiceY();
	void SetIndY(int indY);
	int Get_Contador();
	bool Get_Saltando();
	Direccion GetDireccion();
};
