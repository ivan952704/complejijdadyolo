
#include "stdafx.h"
#include "Nivel.h"

CNivel::CNivel()
{
}

CNivel::~CNivel()
{
}

void CNivel::CargarNivelDeArchivo(FILE *f)
{
	fread(&CodigoNivel, sizeof(int), 1, f);
	fread(&PuntosEnemigo, sizeof(int), 1, f);
	fread(&FrecuenciaEnemigos, sizeof(int), 1, f);
	fread(&CantidadObstaculos, sizeof(int), 1, f);
}

void CNivel::GuardarNivelEnArchivo(FILE *f)
{
	fwrite(&CodigoNivel, sizeof(int), 1, f);
	fwrite(&PuntosEnemigo, sizeof(int), 1, f);
	fwrite(&FrecuenciaEnemigos, sizeof(int), 1, f);
	fwrite(&CantidadObstaculos, sizeof(int), 1, f);
}

int CNivel::GetCodigoNivel()
{
	return CodigoNivel;
}

void CNivel::SetCodigoNivel(int codigo)
{
	CodigoNivel = codigo;
}

int CNivel::GetPuntosEnemigo()
{
	return PuntosEnemigo;
}

void CNivel::SetPuntosEnemigo(int puntos)
{
	PuntosEnemigo = puntos;
}

int CNivel::GetFrecuenciaEnemigos()
{
	return FrecuenciaEnemigos;
}

void CNivel::SetFrecuenciaEnemigos(int frecuencia)
{
	FrecuenciaEnemigos = frecuencia;
}

int CNivel::GetCantidadObstaculos()
{
	return CantidadObstaculos;
}

void CNivel::SetCantidadObstaculos(int cantidad)
{
	CantidadObstaculos = cantidad;
}
