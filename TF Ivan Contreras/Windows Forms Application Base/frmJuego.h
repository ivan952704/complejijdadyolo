#pragma once

#include "Juego.h"
#include <vector>
#include <algorithm>

namespace WindowsFormsApplicationBase
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;

	public ref class frmJuego : public System::Windows::Forms::Form
	{
	private: System::Windows::Forms::PictureBox^  pbxEliminadoA;
	private: System::Windows::Forms::Timer^  timerRetroceso;
	private: System::Windows::Forms::PictureBox^  pbxEnemigoB;
	private: System::Windows::Forms::Timer^  timer_tiempo;
	private: System::Windows::Forms::PictureBox^  pbxEnemigoC;
	private: System::Windows::Forms::PictureBox^  pbxAtaque;
	private: System::Windows::Forms::Timer^  timerJuego;
	private: System::Windows::Forms::PictureBox^  pbxEnemigoA;
	private: System::Windows::Forms::PictureBox^  pbxMapa2;
	private: System::Windows::Forms::PictureBox^  pbxMapa1;
	private: System::Windows::Forms::PictureBox^  pbxMapa3;
	private: System::Windows::Forms::PictureBox^  pbxPersonaje;
	private: System::ComponentModel::IContainer^  components;

	private:
		Direccion dir;
		Bitmap ^imgPersonaje;
		Bitmap ^imgEnemigoA;
		Bitmap ^imgEnemigoB;
		Bitmap ^imgEnemigoC;
		Bitmap ^imgAtaque;
		Bitmap ^imgEliminadoA;
		int cont_truco;
		int cont_enemigos;
		int cont_fuga;
		int *pxA;
		int *pyA;
		int cont_muerte_A;
		int min, seg;
		int centroMapa;
		bool gameOver;
		int indiceEnemigoCercano;

	public:
		CJuego *objJuego;
		bool autoPlay;

		frmJuego(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			dir = Direccion::Ninguno;
			objJuego = NULL;
			cont_truco = 0;
			cont_enemigos = 10;
			cont_fuga = 20;
			pxA = new int;
			pyA = new int;
			*pxA = -1;
			*pyA = -1;
			cont_muerte_A = 0;
			seg = 1;
			min = 1;
			autoPlay = false;
			centroMapa = 450;
			gameOver = false;
			indiceEnemigoCercano = -1;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~frmJuego()
		{
			if (components)
			{
				delete components;
			}
			delete imgPersonaje;
			delete imgEnemigoA;
			delete imgEliminadoA;
			delete imgEnemigoB;
			delete imgEnemigoC;
			delete imgAtaque;
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmJuego::typeid));
			this->timerJuego = (gcnew System::Windows::Forms::Timer(this->components));
			this->pbxPersonaje = (gcnew System::Windows::Forms::PictureBox());
			this->pbxEnemigoA = (gcnew System::Windows::Forms::PictureBox());
			this->pbxMapa2 = (gcnew System::Windows::Forms::PictureBox());
			this->pbxMapa1 = (gcnew System::Windows::Forms::PictureBox());
			this->pbxMapa3 = (gcnew System::Windows::Forms::PictureBox());
			this->pbxEliminadoA = (gcnew System::Windows::Forms::PictureBox());
			this->timerRetroceso = (gcnew System::Windows::Forms::Timer(this->components));
			this->pbxEnemigoB = (gcnew System::Windows::Forms::PictureBox());
			this->timer_tiempo = (gcnew System::Windows::Forms::Timer(this->components));
			this->pbxEnemigoC = (gcnew System::Windows::Forms::PictureBox());
			this->pbxAtaque = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxPersonaje))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoA))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEliminadoA))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoC))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxAtaque))->BeginInit();
			this->SuspendLayout();
			// 
			// timerJuego
			// 
			this->timerJuego->Enabled = true;
			this->timerJuego->Interval = 30;
			this->timerJuego->Tick += gcnew System::EventHandler(this, &frmJuego::timerJuego_Tick);
			// 
			// pbxPersonaje
			// 
			this->pbxPersonaje->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxPersonaje.Image")));
			this->pbxPersonaje->Location = System::Drawing::Point(36, 46);
			this->pbxPersonaje->Name = L"pbxPersonaje";
			this->pbxPersonaje->Size = System::Drawing::Size(184, 68);
			this->pbxPersonaje->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxPersonaje->TabIndex = 0;
			this->pbxPersonaje->TabStop = false;
			this->pbxPersonaje->Visible = false;
			// 
			// pbxEnemigoA
			// 
			this->pbxEnemigoA->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxEnemigoA.Image")));
			this->pbxEnemigoA->Location = System::Drawing::Point(36, 135);
			this->pbxEnemigoA->Name = L"pbxEnemigoA";
			this->pbxEnemigoA->Size = System::Drawing::Size(100, 50);
			this->pbxEnemigoA->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxEnemigoA->TabIndex = 1;
			this->pbxEnemigoA->TabStop = false;
			this->pbxEnemigoA->Visible = false;
			// 
			// pbxMapa2
			// 
			this->pbxMapa2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxMapa2.Image")));
			this->pbxMapa2->Location = System::Drawing::Point(36, 213);
			this->pbxMapa2->Name = L"pbxMapa2";
			this->pbxMapa2->Size = System::Drawing::Size(100, 50);
			this->pbxMapa2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxMapa2->TabIndex = 2;
			this->pbxMapa2->TabStop = false;
			this->pbxMapa2->Visible = false;
			// 
			// pbxMapa1
			// 
			this->pbxMapa1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxMapa1.Image")));
			this->pbxMapa1->Location = System::Drawing::Point(36, 282);
			this->pbxMapa1->Name = L"pbxMapa1";
			this->pbxMapa1->Size = System::Drawing::Size(100, 50);
			this->pbxMapa1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxMapa1->TabIndex = 3;
			this->pbxMapa1->TabStop = false;
			this->pbxMapa1->Visible = false;
			// 
			// pbxMapa3
			// 
			this->pbxMapa3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxMapa3.Image")));
			this->pbxMapa3->Location = System::Drawing::Point(36, 357);
			this->pbxMapa3->Name = L"pbxMapa3";
			this->pbxMapa3->Size = System::Drawing::Size(100, 50);
			this->pbxMapa3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxMapa3->TabIndex = 4;
			this->pbxMapa3->TabStop = false;
			this->pbxMapa3->Visible = false;
			// 
			// pbxEliminadoA
			// 
			this->pbxEliminadoA->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxEliminadoA.Image")));
			this->pbxEliminadoA->Location = System::Drawing::Point(257, 46);
			this->pbxEliminadoA->Name = L"pbxEliminadoA";
			this->pbxEliminadoA->Size = System::Drawing::Size(100, 50);
			this->pbxEliminadoA->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxEliminadoA->TabIndex = 5;
			this->pbxEliminadoA->TabStop = false;
			this->pbxEliminadoA->Visible = false;
			// 
			// timerRetroceso
			// 
			this->timerRetroceso->Tick += gcnew System::EventHandler(this, &frmJuego::timerTiempo_Tick);
			// 
			// pbxEnemigoB
			// 
			this->pbxEnemigoB->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxEnemigoB.Image")));
			this->pbxEnemigoB->Location = System::Drawing::Point(257, 125);
			this->pbxEnemigoB->Name = L"pbxEnemigoB";
			this->pbxEnemigoB->Size = System::Drawing::Size(100, 60);
			this->pbxEnemigoB->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxEnemigoB->TabIndex = 6;
			this->pbxEnemigoB->TabStop = false;
			this->pbxEnemigoB->Visible = false;
			// 
			// timer_tiempo
			// 
			this->timer_tiempo->Interval = 1000;
			this->timer_tiempo->Tick += gcnew System::EventHandler(this, &frmJuego::timer_tiempo_Tick);
			// 
			// pbxEnemigoC
			// 
			this->pbxEnemigoC->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxEnemigoC.Image")));
			this->pbxEnemigoC->Location = System::Drawing::Point(410, 125);
			this->pbxEnemigoC->Name = L"pbxEnemigoC";
			this->pbxEnemigoC->Size = System::Drawing::Size(194, 60);
			this->pbxEnemigoC->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxEnemigoC->TabIndex = 10;
			this->pbxEnemigoC->TabStop = false;
			this->pbxEnemigoC->Visible = false;
			// 
			// pbxAtaque
			// 
			this->pbxAtaque->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxAtaque.Image")));
			this->pbxAtaque->Location = System::Drawing::Point(410, 46);
			this->pbxAtaque->Name = L"pbxAtaque";
			this->pbxAtaque->Size = System::Drawing::Size(92, 17);
			this->pbxAtaque->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxAtaque->TabIndex = 9;
			this->pbxAtaque->TabStop = false;
			this->pbxAtaque->Visible = false;
			// 
			// frmJuego
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(934, 461);
			this->Controls->Add(this->pbxEnemigoC);
			this->Controls->Add(this->pbxAtaque);
			this->Controls->Add(this->pbxEnemigoB);
			this->Controls->Add(this->pbxEliminadoA);
			this->Controls->Add(this->pbxMapa3);
			this->Controls->Add(this->pbxMapa1);
			this->Controls->Add(this->pbxMapa2);
			this->Controls->Add(this->pbxEnemigoA);
			this->Controls->Add(this->pbxPersonaje);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(950, 500);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(950, 500);
			this->Name = L"frmJuego";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"BRAID THE GAME";
			this->Load += gcnew System::EventHandler(this, &frmJuego::frmJuego_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &frmJuego::frmJuego_Paint);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &frmJuego::frmJuego_KeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxPersonaje))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoA))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxMapa3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEliminadoA))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxEnemigoC))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxAtaque))->EndInit();
			this->ResumeLayout(false);

		}

	private: void FormSiguienteNivel()
	{
		timerJuego->Enabled = false;
		timerRetroceso->Enabled = false;
		objJuego->Siguiente_Nivel();
		MessageBox::Show("Prepárese para el siguiente nivel!!!", "Braid The Game",
			MessageBoxButtons::OK, MessageBoxIcon::Information);
		timerJuego->Enabled = true;
		timerRetroceso->Enabled = true;
	}

	private: System::Void frmJuego_Load(System::Object^  sender, System::EventArgs^  e) {
		imgPersonaje = gcnew Bitmap(this->pbxPersonaje->Image);
		imgPersonaje->MakeTransparent(imgPersonaje->GetPixel(1, 1));
		imgEnemigoA = gcnew Bitmap(this->pbxEnemigoA->Image);
		imgEnemigoA->MakeTransparent(imgEnemigoA->GetPixel(1, 1));
		imgEliminadoA = gcnew Bitmap(this->pbxEliminadoA->Image);
		imgEliminadoA->MakeTransparent(imgEliminadoA->GetPixel(1, 1));
		imgEnemigoB = gcnew Bitmap(this->pbxEnemigoB->Image);
		imgEnemigoB->MakeTransparent(imgEnemigoB->GetPixel(1, 1));
		imgEnemigoC = gcnew Bitmap(this->pbxEnemigoC->Image);
		imgEnemigoC->MakeTransparent(imgEnemigoC->GetPixel(1, 1));
		imgAtaque = gcnew Bitmap(this->pbxAtaque->Image);
		imgAtaque->MakeTransparent(imgAtaque->GetPixel(1, 1));
		timerRetroceso->Enabled = true;
	}

	private: System::Void frmJuego_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e)
	{
		Graphics ^g = this->CreateGraphics();
		g->Clear(Color::Cyan);
		delete g;
	}

	private: System::Void timerTiempo_Tick(System::Object^  sender, System::EventArgs^  e) {
		objJuego->GuardarPosiciones();
	}

	private: System::Void timer_tiempo_Tick(System::Object^  sender, System::EventArgs^  e) {
		if (min >= 0)
		{
			seg--;
			if (seg == -1)
			{
				seg = 59;
				min--;
			}
		}
	}

	private: System::Void frmJuego_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (timerJuego->Enabled == true)
		{
			if (e->KeyCode == Keys::Left && dir != Direccion::SaltoDerecha && dir != Direccion::SaltoIzquierda && !objJuego->Get_Saltando_Personaje())
			{
				if (dir != Direccion::Izquierda)
				{
					dir = Direccion::Izquierda;
					objJuego->Cambiar_Direccion_Personaje(dir);
				}
			}
			if (e->KeyCode == Keys::Right && dir != Direccion::SaltoDerecha && dir != Direccion::SaltoIzquierda && !objJuego->Get_Saltando_Personaje())
			{
				if (dir != Direccion::Derecha)
				{
					dir = Direccion::Derecha;
					objJuego->Cambiar_Direccion_Personaje(dir);
				}
			}
			if (e->KeyCode == Keys::Space && objJuego->Get_IndiceX_Personaje() == 0)
			{
				dir = Direccion::SaltoDerecha;
				objJuego->Cambiar_Direccion_Personaje(dir);
			}
			if (e->KeyCode == Keys::Space && objJuego->Get_IndiceX_Personaje() == 1)
			{
				dir = Direccion::SaltoIzquierda;
				objJuego->Cambiar_Direccion_Personaje(dir);
			}
			if (e->KeyCode == Keys::Down && dir != Direccion::Ninguno && dir != Direccion::SaltoDerecha && dir != Direccion::SaltoIzquierda)
			{
				dir = Direccion::Ninguno;
				objJuego->Cambiar_Direccion_Personaje(dir);
			}
			if (e->KeyCode == Keys::S)
			{
				if (!objJuego->Get_Saltando_Personaje())
					objJuego->Inicilizar_Salto_Personaje();
			}
			if (e->KeyCode == Keys::R)
			{
				objJuego->RetrocesoTiempo();
			}
			if (e->KeyCode == Keys::N)
			{
				if (cont_truco == 0)
					cont_truco++;
				else
					cont_truco = 0;
			}
			if (e->KeyCode == Keys::E)
			{
				if (cont_truco == 1)
					cont_truco++;
				else
					cont_truco = 0;
			}
			if (e->KeyCode == Keys::X)
			{
				if (cont_truco == 2)
					cont_truco++;
				else
					cont_truco = 0;
			}
			if (e->KeyCode == Keys::T)
			{
				if (cont_truco == 3)
				{
					timerJuego->Enabled = false;
					timerRetroceso->Enabled = false;
					objJuego->Siguiente_Nivel();
					if (objJuego->GetNivelActual() == objJuego->GetNroNiveles() + 1)//
					{
						MessageBox::Show("Felicidades usted a completado el juego!!!", "Braid The Game",
							MessageBoxButtons::OK, MessageBoxIcon::Information);
						this->Close();
					}
					else
					{
						MessageBox::Show("Prepárese para el siguiente nivel!!!", "Braid The Game",
							MessageBoxButtons::OK, MessageBoxIcon::Information);
						timerJuego->Enabled = true;
						timerRetroceso->Enabled = true;
					}
				}
				cont_truco = 0;
			}
		}
		if (e->KeyCode == Keys::P)
		{
			timerJuego->Enabled = !timerJuego->Enabled;
			timerRetroceso->Enabled = !timerRetroceso->Enabled;
			System::Drawing::Font ^fnt = gcnew System::Drawing::Font("Chiller", 100, FontStyle::Bold);
			Graphics ^g = this->CreateGraphics();
			g->DrawString("PAUSA", fnt, Brushes::Red, this->Width / 4, this->Height / 4);
			delete g;
		}
		if (e->KeyCode == Keys::Escape)
		{
			timerJuego->Enabled = false;
			timerRetroceso->Enabled = false;
			timer_tiempo->Enabled = false;
			objJuego->Inicializar_Todo();
			this->Close();
		}
	}

	private: void MoverPersonajeCentroMapa()
	{
		int x = objJuego->Get_X_Personaje();

		if (x < centroMapa)
			SendKeys::Send("{Right}");
		else if (x > centroMapa)
			SendKeys::Send("{Left}");
	}

	private: bool esMovimientoSeguro(int indice, Direccion direccion)
	{
		return !objJuego->ExisteColision(indice, direccion);
	}

	private: bool BackTrack(Direccion direccion, int indice)
	{
		if (objJuego->Get_Cantidad_Enemigos() == 0)
		{
			MoverPersonajeCentroMapa();
			return true;
		}

		for (int i = indice; i < objJuego->Get_Cantidad_Enemigos(); i++)
		{
			Direccion dirEnemigo = objJuego->Get_Direccion_Enemigo(i);

			if (esMovimientoSeguro(i, direccion))
			{
				if (dirEnemigo == Direccion::Derecha)
					SendKeys::Send("{Right}");
				else
					SendKeys::Send("{Left}");

				Direccion siguiente = direccion == Direccion::Derecha ? Direccion::Izquierda : Direccion::Derecha;

				if (BackTrack(siguiente, indice + 1))
					return true;
			}
			else
			{
				if (objJuego->GetNivelActual() > 1)
					SendKeys::Send("S");
				else
					SendKeys::Send(" ");
			}

			if (objJuego->GetNivelActual() > 1)
				SendKeys::Send("{Down}");
		}

		return false;
	}

	private: void Greedy()
	{
		if (objJuego->Get_Cantidad_Enemigos() == 0)
		{
			MoverPersonajeCentroMapa();
			indiceEnemigoCercano = -1;
			return;
		}

		Direccion direccion = Direccion::Ninguno;
		vector<int> posicionEnemigos;

		for (int i = 0; i < objJuego->Get_Cantidad_Enemigos(); i++)
		{
			int xPersonaje = objJuego->Get_X_Personaje();
			int xEnemigo = objJuego->GetXEnemigo(i);

			if (xEnemigo < xPersonaje)
				posicionEnemigos.push_back(xPersonaje - xEnemigo);
			else
				posicionEnemigos.push_back(xEnemigo - xPersonaje);
		}

		auto minimo = std::min_element(posicionEnemigos.begin(), posicionEnemigos.end());
		indiceEnemigoCercano = distance(posicionEnemigos.begin(), minimo);
		direccion = objJuego->Get_Direccion_Enemigo(indiceEnemigoCercano);

		if (dir == Direccion::Ninguno)
		{
			if (direccion == Direccion::Izquierda)
				SendKeys::Send("{Left}");
			else
				SendKeys::Send("{Right}");
		}

		if (indiceEnemigoCercano != -1)
		{
			if (objJuego->ExisteColision(indiceEnemigoCercano, direccion))
			{
				if (objJuego->GetNivelActual() > 1)
					SendKeys::Send("S");
				else
 					SendKeys::Send(" ");
				
				indiceEnemigoCercano = -1;
			}
		}

		if (objJuego->GetNivelActual() > 1)
			SendKeys::Send("{Down}");
	}

	private: System::Void timerJuego_Tick(System::Object^  sender, System::EventArgs^  e)
	{
		Graphics ^g = this->CreateGraphics();

		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;

		BufferedGraphicsContext ^espacioBuffer = BufferedGraphicsManager::Current;
		espacioBuffer->MaximumBuffer = System::Drawing::Size(gWidth + 1, gHeigth + 1);
		BufferedGraphics ^buffer = espacioBuffer->Allocate(g, Drawing::Rectangle(0, 0, gWidth, gHeigth));

		buffer->Graphics->Clear(Color::Cyan);

		//fondo
		System::Drawing::Font ^fnt = gcnew System::Drawing::Font("Chiller", 40, FontStyle::Bold);
		switch (objJuego->GetNivelActual())
		{
		case 1:
			buffer->Graphics->DrawImage(pbxMapa1->Image, 0, 0, gWidth, gHeigth);

			buffer->Graphics->DrawString("Enemigos Eliminados " + System::Convert::ToString(objJuego->GetEnemigosEliminados())
				+ "/" + System::Convert::ToString(cont_enemigos), fnt, Brushes::Black, gWidth / 3, 5);

			if (objJuego->GetEnemigosEliminados() >= cont_enemigos)
				FormSiguienteNivel();
			break;
		case 2:
			buffer->Graphics->DrawImage(pbxMapa2->Image, 0, 0, gWidth, gHeigth);
			buffer->Graphics->DrawString("Fuga de Enemigos " + System::Convert::ToString(objJuego->GetEnemigosEliminados())
				+ "/" + System::Convert::ToString(cont_fuga), fnt, Brushes::Black, gWidth / 3, 5);

			if (objJuego->GetEnemigosEliminados() >= cont_fuga)
				FormSiguienteNivel();
			break;
		case 3:
			if (timer_tiempo->Enabled == false)
				timer_tiempo->Enabled = true;
			buffer->Graphics->DrawImage(pbxMapa3->Image, 0, 0, gWidth, gHeigth);

			buffer->Graphics->DrawString("Tiempo de supervivencia " + System::Convert::ToString(min), fnt, Brushes::Black, gWidth / 3, 5);
			buffer->Graphics->DrawString(" : " + System::Convert::ToString(seg), fnt, Brushes::Black, gWidth - 180, 5);

			if (min == 0 && seg == 0)
			{
				timerJuego->Enabled = false;
				timerRetroceso->Enabled = false;
				System::Random ^r = gcnew System::Random(System::DateTime::Now.Ticks);
				objJuego->SetPuntosGanados(objJuego->GetPuntosGanados() + r->Next(500, 2001));
				objJuego->Inicializar_Todo();
				MessageBox::Show("Felicidades usted a completado el juego!!!", "Braid The Game",
					MessageBoxButtons::OK, MessageBoxIcon::Information);
				this->Close();
			}
			break;
		}
		buffer->Graphics->DrawString("Puntos: " + System::Convert::ToString(objJuego->GetPuntosGanados()), fnt, Brushes::Black, 20, 5);
		buffer->Graphics->DrawString("Retroceso: " + System::Convert::ToString(objJuego->GetIteraciones()) + " iteraciones", fnt, Brushes::Black, 20, 50);
		//

		objJuego->Agregar_Enemigos();
		objJuego->Enemigo_Agrega_Misil();
		objJuego->Eliminar_Enemigos(buffer->Graphics);

		if (autoPlay)
		{
			BackTrack(Direccion::Izquierda, 0);
			//Greedy();
		}

		objJuego->Personaje_Elimina_Enemigo(pxA, pyA);

		objJuego->Mover_Personaje(buffer->Graphics);
		objJuego->Personaje_Salta();
		objJuego->Pintar_Personaje(buffer->Graphics, imgPersonaje);

		objJuego->Mover_Enemigos(buffer->Graphics);
		objJuego->Enemigo_Mueve_Misiles(buffer->Graphics);
		objJuego->Pintar_Enemigos(buffer->Graphics, imgEnemigoA, imgEnemigoB, imgEnemigoC);
		objJuego->Enemigo_Pinta_Misiles(buffer->Graphics, imgAtaque);
		objJuego->Enemigo_Elimina_Misil();

		if (cont_muerte_A == 8)
		{
			cont_muerte_A = 0;
			*pxA = -1;
			*pyA = -1;
		}
		if (*pxA != -1 && *pyA != -1)
		{
			cont_muerte_A++;
			buffer->Graphics->DrawImage(imgEliminadoA, *pxA, *pyA, 60, 80);
		}

		if (objJuego->Get_Direccion_Personaje() == Direccion::Ninguno)
			dir = Direccion::Ninguno;

		if ((objJuego->Get_Direccion_Personaje() == Direccion::Izquierda
			|| objJuego->Get_Direccion_Personaje() == Direccion::Derecha)
			&& objJuego->Get_Y_Personaje() != 260)
			objJuego->SetPersonajeY(260);

		buffer->Render(g);

		delete buffer;
		delete espacioBuffer;
		delete g;
	}
	};
}
