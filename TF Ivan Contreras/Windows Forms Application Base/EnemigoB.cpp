
#include "stdafx.h"
//#include "EnemigoB.h"

CEnemigoB::CEnemigoB(int x, int y, int dir) : CEnemigo()
{
	this->x = x;
	this->y = y;
	dirEnemigo = dir;
	dy = 5;
	contSubida = contIndice = 0;
	golpe = false;

	if (dir == 0)
	{
		indX = indY = 0;
		dx = -3;
	}
	else
	{
		indX = 3;
		indY = 2;
		dx = 3;
	}
}

CEnemigoB::~CEnemigoB()
{
}

void CEnemigoB::Pintar(Graphics ^g, Bitmap ^img)
{
	Rectangle porcion_imagen;
	if (golpe)
	{
		w = img->Width / 2;
		h = img->Height / 4;

		porcion_imagen = Rectangle(w * indX, h * indY, w, h);
		w = 120;
		h = 80;
	}
	else
	{
		w = img->Width / 4;
		h = img->Height / 4;

		porcion_imagen = Rectangle(w * indX, h * indY, w, h);
		w = 60;
		h = 80;
	}

	
	Rectangle zoom = Rectangle(x, y, w, h);
	g->DrawImage(img, zoom, porcion_imagen, GraphicsUnit::Pixel);

	contIndice++;
	if (dirEnemigo == 0)
	{
		if (!golpe)
		{
			if (contIndice == 5)
			{
				indX++;
				contIndice = 0;
			}
			if (indX == 4)
			{
				//indX = 0;
				golpe = true;
				indX = 0;
				indY = 1;
			}
		}
		else
		{
			if (contIndice == 5)
			{
				indX = indY = 0;
				contIndice = 0;
				golpe = false;
			}
		}
	}
	else
	{
		if (!golpe)
		{
			if (contIndice == 5)
			{
				indX--;
				contIndice = 0;
			}
			if (indX == -1)
			{
				//indX = 3;
				golpe = true;
				indX = 1;
				indY = 3;
			}
		}
		else
		{
			if (contIndice == 5)
			{
				indX = 3;
				indY = 2;
				contIndice = 0;
				golpe = false;
			}
		}
	}
}

void CEnemigoB::Mover(Graphics ^g)
{
	x += dx;
	y += dy;
	contSubida++;
	if (contSubida == 12)
	{
		dy *= -1;
		contSubida = 0;
	}
}
