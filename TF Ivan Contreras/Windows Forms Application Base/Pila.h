
#include "Nodo.h"

class CPila
{
private:
	CNodo *first;
	int nele;
public:
	CPila();
	~CPila();

	void Push(float x, float y, int indX, int indY);
	bool Pop();
	CNodo *Top();
	bool Empty();
	int Size();
	void Clear();
};
