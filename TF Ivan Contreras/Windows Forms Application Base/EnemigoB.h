
//#include "Enemigo.h"

class CEnemigoB : public CEnemigo
{
private:
	int contSubida, contIndice;
	bool golpe;
public:
	CEnemigoB(int x, int y, int dir);
	~CEnemigoB();

	void Pintar(Graphics ^g, Bitmap ^img);
	void Mover(Graphics ^g);
};
