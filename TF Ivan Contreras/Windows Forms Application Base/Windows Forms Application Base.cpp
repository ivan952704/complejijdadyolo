// Windows Forms Application Base.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"
#include "frmJuego.h"
#include "frmMenu.h"

using namespace WindowsFormsApplicationBase;

[STAThreadAttribute]
int main()
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	//Application::Run(gcnew Form1());
	Application::Run(gcnew Form1());
	return 0;
}
