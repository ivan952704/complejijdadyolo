
//#include "Enemigo.h"

class CEnemigoA : public CEnemigo
{
public:
	CEnemigoA(int x, int y, int dir);
	~CEnemigoA();

	void Pintar(Graphics ^g, Bitmap ^img);
	void Mover(Graphics ^g);
};
