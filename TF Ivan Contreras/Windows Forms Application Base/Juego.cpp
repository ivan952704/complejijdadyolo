
#include "stdafx.h"
#include "Juego.h"
#include <stdio.h>

CJuego::CJuego()
{
	objPersonaje = new CPersonaje();
	movimientos = new CPila();
	ArregloEnemigos = NULL;
	NroEnemigos = AparicionEnemigos = NivelActual = 0;
	NivelDesbloqueado = 1;
	puntos_ganados = 0;
	EnemigosEliminados = 0;
	retroceso = false;
	iteraciones = 0;
}

CJuego::~CJuego()
{
	for (int i = 0; i < NroEnemigos; i++)
		delete ArregloEnemigos[i];

	if (ArregloEnemigos != NULL)
		delete[]ArregloEnemigos;

	delete objPersonaje;
}

void CJuego::Pintar_Personaje(Graphics ^g, Bitmap ^img)
{
	if (NivelActual == 0)
		return;

	objPersonaje->CPersonaje::Pintar(g, img);
}

void CJuego::Mover_Personaje(Graphics ^g)
{
	if (NivelActual == 0)
		return;

	objPersonaje->CPersonaje::Mover(g);
}

void CJuego::Cambiar_Direccion_Personaje(Direccion dir)
{
	if (NivelActual == 0)
		return;

	objPersonaje->CPersonaje::CambioDireccion(dir);
}

void CJuego::Mover_Enemigos(Graphics ^g)
{
	if (retroceso)
		return;

	for (int i = 0; i < NroEnemigos; i++)
		ArregloEnemigos[i]->Mover(g);
}

void CJuego::Pintar_Enemigos(Graphics ^g, Bitmap ^imgA, Bitmap ^imgB, Bitmap ^imgC)
{
	for (int i = 0; i < NroEnemigos; i++)
	{
		if (dynamic_cast<CEnemigoA*>(ArregloEnemigos[i]))
			ArregloEnemigos[i]->Pintar(g, imgA);
		if (dynamic_cast<CEnemigoB*>(ArregloEnemigos[i]))
			ArregloEnemigos[i]->Pintar(g, imgB);
		if (dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]))
			ArregloEnemigos[i]->Pintar(g, imgC);
	}
}

void CJuego::Agregar_Enemigos()
{
	if (retroceso)
		return;

	if (NivelActual == 0)
		return;

	AparicionEnemigos++;
	int intervalo = VectorNiveles[NivelActual-1]->GetFrecuenciaEnemigos();

	if (AparicionEnemigos <= intervalo)
		return;

	CEnemigo **temp = new CEnemigo*[NroEnemigos + 1];

	for (int i = 0; i < NroEnemigos; i++)
		temp[i] = ArregloEnemigos[i];

	System::Random ^r = gcnew System::Random(System::DateTime::Now.Ticks);
	int aux = r->Next(1, 21);
	int px, dir;

	dir = aux <= 10 ? 0 : 1;

	switch (NivelActual)
	{
	case 1:
		px = dir == 0 ? px = 0 : px = 850;
		temp[NroEnemigos] = new CEnemigoA(px, 270, dir);
		break;
	case 2:
		px = dir == 0 ? px = 850 : px = 0;
		temp[NroEnemigos] = new CEnemigoB(px, 260, dir);
		break;
	case 3:
		px = dir == 0 ? px = 850 : px = 0;
		temp[NroEnemigos] = new CEnemigoC(px, 100, dir);
		break;
	}

	NroEnemigos += 1;
	if (ArregloEnemigos != NULL)
		delete[]ArregloEnemigos;

	ArregloEnemigos = temp;
	AparicionEnemigos = 0;
}

void CJuego::Eliminar_Enemigos(Graphics ^g)
{
	if (NroEnemigos > 0)
	{
		int pos = -1;
		for (int i = 0; i < NroEnemigos; i++)
		{
			if (ArregloEnemigos[i]->GetX() < 0 || ArregloEnemigos[i]->GetXW() > g->VisibleClipBounds.Width)
			{
				pos = i;
				if (NivelActual == 2)
				{
					EnemigosEliminados++;
					puntos_ganados += VectorNiveles[NivelActual - 1]->GetPuntosEnemigo();
				}
				break;
			}
		}
		EliminaEnemigoPosicion(pos);
	}
}

void CJuego::Enemigo_Agrega_Misil()
{
	if (retroceso)
		return;

	if (NivelActual != 0)
	{
		if (VectorNiveles[NivelActual - 1]->GetCodigoNivel() == 3)
		{
			for (int i = 0; i < NroEnemigos; i++)
			{
				CEnemigoC *obj = dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]);
				obj->CEnemigoC::Agregar_Bala();
			}
		}
	}
}

void CJuego::Enemigo_Elimina_Misil()
{
	if (NivelActual != 0)
	{
		if (VectorNiveles[NivelActual - 1]->GetCodigoNivel() == 3)
		{
			for (int i = 0; i < NroEnemigos; i++)
			{
				CEnemigoC *obj = dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]);
				obj->CEnemigoC::Eliminar_Bala();
			}
		}
	}
}

void CJuego::Enemigo_Pinta_Misiles(Graphics ^g, Bitmap ^img)
{
	if (NivelActual != 0)
	{
		if (VectorNiveles[NivelActual - 1]->GetCodigoNivel() == 3)
		{
			for (int i = 0; i < NroEnemigos; i++)
			{
				CEnemigoC *obj = dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]);
				obj->CEnemigoC::Pintar_Balas(g, img);
			}
		}
	}
}

void CJuego::Enemigo_Mueve_Misiles(Graphics ^g)
{
	if (retroceso)
		return;

	if (NivelActual != 0)
	{
		if (VectorNiveles[NivelActual - 1]->GetCodigoNivel() == 3)
		{
			for (int i = 0; i < NroEnemigos; i++)
			{
				CEnemigoC *obj = dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]);
				obj->CEnemigoC::Mover_Balas(g);
			}
		}
	}
}

bool CJuego::Enemigo_Elimina_Personaje()
{
	if (!retroceso && NroEnemigos > 0 && !objPersonaje->Get_Saltando() && objPersonaje->GetIndiceY() < 3)
	{
		for (int i = 0; i < NroEnemigos; i++)
		{
			if (NivelActual > 1 && ArregloEnemigos[i]->GetX() <= objPersonaje->GetXW() - 25 &&
				ArregloEnemigos[i]->GetXW() - 25 >= objPersonaje->GetX() &&
				ArregloEnemigos[i]->GetY() <= objPersonaje->GetYH() &&
				ArregloEnemigos[i]->GetYH() >= objPersonaje->GetY())
				return true;

			if (NivelActual == 1 && ArregloEnemigos[i]->GetX() <= objPersonaje->GetXW() - 25 &&
				ArregloEnemigos[i]->GetXW() - 25 >= objPersonaje->GetX())
					return true;
		}
	}
	return false;
}

void CJuego::Personaje_Elimina_Enemigo(int *px, int *py)
{
	if (NroEnemigos > 0 &&  NivelActual == 1 && !objPersonaje->Get_Saltando() && objPersonaje->GetIndiceY() >= 3)
	{
		int pos = -1;
		for (int i = 0; i < NroEnemigos; i++)
		{
			if (ArregloEnemigos[i]->GetX() < objPersonaje->GetXW() -20 &&
				ArregloEnemigos[i]->GetXW() >= objPersonaje->GetX() &&
				ArregloEnemigos[i]->GetY() <= objPersonaje->GetYH() &&
				ArregloEnemigos[i]->GetYH() - 20 >= objPersonaje->GetY())
			{
				pos = i;
				puntos_ganados += VectorNiveles[NivelActual - 1]->GetPuntosEnemigo();
				EnemigosEliminados++;
				*px = ArregloEnemigos[pos]->GetX();
				*py = ArregloEnemigos[pos]->GetY();
				break;
			}
		}
		EliminaEnemigoPosicion(pos);
	}
}

bool CJuego::Misil_Elimina_Personaje()
{
	if (NivelActual == 0)
		return false;

	if (!retroceso && VectorNiveles[NivelActual - 1]->GetCodigoNivel() == 3 && NroEnemigos > 0)
	{
		for (int i = 0; i<NroEnemigos; i++)
		{
			CEnemigoC *obj = dynamic_cast<CEnemigoC*>(ArregloEnemigos[i]);
			for (int i = 0; i < obj->CEnemigoC::GetNumeroMisiles(); i++)
			{
				if (obj->GetMisil(i)->GetX() <= objPersonaje->GetXW() - 15 &&
					obj->GetMisil(i)->GetXW() >= objPersonaje->GetX() &&
					obj->GetMisil(i)->GetY() <= objPersonaje->GetYH()  - 15 &&
					obj->GetMisil(i)->GetYH() >= objPersonaje->GetY())
					return true;
			}
		}
		return false;
	}
}

int CJuego::Get_Cantidad_Enemigos()
{
	return NroEnemigos;
}

bool CJuego::ExisteColision(int indice, Direccion direccion)
{
	if (indice >= Get_Cantidad_Enemigos())
		return false;

	CEnemigo *objEnemigo = ArregloEnemigos[indice];
	int adicional = 0;

	if (NivelActual > 1)
		adicional = direccion == Direccion::Izquierda ? -40 : 40;
	else
		adicional = direccion == Direccion::Izquierda ? -30 : 30;

	if (NivelActual > 1 && objEnemigo->GetX() + adicional <= objPersonaje->GetXW() - 25 &&
		objEnemigo->GetXW() - 25 >= objPersonaje->GetX() + adicional &&
		objEnemigo->GetY() <= objPersonaje->GetYH() &&
		objEnemigo->GetYH() >= objPersonaje->GetY())
		return true;

	if (NivelActual == 1 && objEnemigo->GetX() + adicional <= objPersonaje->GetXW() - 25 &&
		objEnemigo->GetXW() - 25 >= objPersonaje->GetX() + adicional)
		return true;

	return false;
}

int CJuego::GetXEnemigo(int indice)
{
	CEnemigo *objEnemigo = ArregloEnemigos[indice];
	return objEnemigo->GetX();
}

void CJuego::GuardarPosiciones()
{
	movimientos->Push(objPersonaje->GetX(), objPersonaje->GetY(),
		objPersonaje->GetIndiceX(), objPersonaje->GetIndiceY());
}

void CJuego::RetrocesoTiempo()
{
	if (!movimientos->Empty())
	{
		CNodo *cabeza = movimientos->Top();
		objPersonaje->SetX(cabeza->GetX());
		objPersonaje->SetY(cabeza->GetY());
		objPersonaje->SetIndX(cabeza->GetIndX());
		objPersonaje->SetIndY(cabeza->GetIndY());
		movimientos->Pop();
		retroceso = true;
		iteraciones++;
		if (movimientos->Empty())
		{
			retroceso = false;
			iteraciones = 0;
		}
	}
}

void CJuego::Inicilizar_Salto_Personaje()
{
	objPersonaje->Inicializar_Salto();
}

bool CJuego::Get_Retroceso()
{
	return retroceso;
}

void CJuego::Personaje_Salta()
{
	switch (NivelActual)
	{
	case 1:
		objPersonaje->Saltar(260);
		break;
	case 2:
		objPersonaje->Saltar(320);
		break;
	case 3:
		objPersonaje->Saltar(235);
		break;
	}
}

void CJuego::EliminaEnemigoPosicion(int pos)
{
	if (pos != -1)
	{
		CEnemigo **Temp = new CEnemigo*[NroEnemigos - 1];

		for (int i = 0; i < pos; i++)
			Temp[i] = ArregloEnemigos[i];
		for (int i = pos; i < NroEnemigos - 1; i++)
			Temp[i] = ArregloEnemigos[i + 1];

		NroEnemigos -= 1;
		delete ArregloEnemigos[pos];
		if (ArregloEnemigos != NULL)
			delete[]ArregloEnemigos;

		ArregloEnemigos = Temp;
	}
}

void CJuego::Siguiente_Nivel()
{
	for (int i = 0; i < NroEnemigos; i++)
		delete ArregloEnemigos[i];

	if (ArregloEnemigos != NULL)
		delete[]ArregloEnemigos;

	delete objPersonaje;

	NroEnemigos = AparicionEnemigos = iteraciones = 0;
	NivelActual++;
	if (NivelDesbloqueado < NroNiveles)
		NivelDesbloqueado++;

	objPersonaje = NULL;
	ArregloEnemigos = NULL;

	objPersonaje = new CPersonaje();
	delete movimientos;
	movimientos = new CPila();
	switch (NivelActual)
	{
	case 1:
		objPersonaje->SetY(260);
		break;
	case 2:
		objPersonaje->SetY(320);
		break;
	case 3:
		objPersonaje->SetY(235);
		break;
	default:
		objPersonaje->SetY(0);
		break;
	}
}

void CJuego::Inicializar_Todo()
{
	for (int i = 0; i < NroEnemigos; i++)
		delete ArregloEnemigos[i];

	if (ArregloEnemigos != NULL)
		delete[]ArregloEnemigos;

	delete objPersonaje;

	EnemigosEliminados = NroEnemigos = AparicionEnemigos = NivelActual = 0;
	iteraciones = 0;
	objPersonaje = NULL;
	ArregloEnemigos = NULL;

	objPersonaje = new CPersonaje();

	delete movimientos;
	movimientos = new CPila();
}

void CJuego::Cargar_Niveles(FILE *f)
{
	for (int i = 0; i < NroNiveles; i++)
	{
		CNivel *objNivel = new CNivel();
		objNivel->CargarNivelDeArchivo(f);
		VectorNiveles.push_back(objNivel);
	}
}

void CJuego::Crear_Niveles(FILE *f)
{
	CNivel *objNivel1 = new CNivel();
	CNivel *objNivel2 = new CNivel();
	CNivel *objNivel3 = new CNivel();
	NroNiveles = 3;

	objNivel1->SetCodigoNivel(1);
	objNivel1->SetFrecuenciaEnemigos(150);
	objNivel1->SetCantidadObstaculos(3);
	objNivel1->SetPuntosEnemigo(50);

	objNivel2->SetCodigoNivel(2);
	objNivel2->SetFrecuenciaEnemigos(150);
	objNivel2->SetCantidadObstaculos(5);
	objNivel2->SetPuntosEnemigo(100);

	objNivel3->SetCodigoNivel(3);
	objNivel3->SetFrecuenciaEnemigos(150);
	objNivel3->SetCantidadObstaculos(8);
	objNivel3->SetPuntosEnemigo(150);

	objNivel1->GuardarNivelEnArchivo(f);
	objNivel2->GuardarNivelEnArchivo(f);
	objNivel3->GuardarNivelEnArchivo(f);

	delete objNivel1;
	delete objNivel2;
	delete objNivel3;
}

void CJuego::Cargar_Partida(FILE *f)
{
	fread(&NivelDesbloqueado, sizeof(int), 1, f);
	fread(&puntos_ganados, sizeof(int), 1, f);
}

void CJuego::Guardar_Partida(FILE *f)
{
	fwrite(&NivelDesbloqueado, sizeof(int), 1, f);
	fwrite(&puntos_ganados, sizeof(int), 1, f);
}

void CJuego::SetNivelActual(int nivel)
{
	NivelActual = nivel;
}

int CJuego::GetPuntosGanados()
{
	return puntos_ganados;
}

void CJuego::SetPuntosGanados(int puntos)
{
	puntos_ganados = puntos;
}

int CJuego::GetEnemigosEliminados()
{
	return EnemigosEliminados;
}

int CJuego::GetNivelActual()
{
	return NivelActual;
}

int CJuego::GetNroNiveles()
{
	return NroNiveles;
}

int CJuego::GetIteraciones()
{
	return iteraciones;
}

Direccion CJuego::Get_Direccion_Personaje()
{
	return objPersonaje->GetDireccion();
}

int CJuego::Get_IndiceX_Personaje()
{
	return objPersonaje->GetIndiceX();
}

int CJuego::Get_X_Personaje()
{
	return objPersonaje->GetX();
}

int CJuego::Get_Y_Personaje()
{
	return objPersonaje->GetY();
}

void CJuego::SetPersonajeY(int y)
{
	objPersonaje->SetY(y);
}

Direccion CJuego::Get_Direccion_Enemigo(int indice)
{
	return ArregloEnemigos[indice]->GetMovimiento() == 0 ? Direccion::Izquierda : Direccion::Derecha;
}

int CJuego::GetNivelDesbloqueado()
{
	return NivelDesbloqueado;
}

void CJuego::SetNivelDesbloqueado(int nivel)
{
	NivelDesbloqueado = nivel;
}

bool CJuego::Get_Saltando_Personaje()
{
	return objPersonaje->Get_Saltando();
}
