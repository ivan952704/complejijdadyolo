
#include "stdafx.h"
#include "Nodo.h"
#include <stdio.h>

CNodo::CNodo()
{
	next = NULL;
	x = y = -1;
	indX = indY = -1;
}

CNodo::~CNodo()
{
}

CNodo *CNodo::GetNext()
{
	return next;
}

void CNodo::SetNext(CNodo *next)
{
	this->next = next;
}

float CNodo::GetX()
{
	return x;
}

void CNodo::SetX(float x)
{
	this->x = x;
}

float CNodo::GetY()
{
	return y;
}

void CNodo::SetY(float y)
{
	this->y = y;
}

int CNodo::GetIndX()
{
	return indX;
}

void CNodo::SetIndX(int indX)
{
	this->indX = indX;
}

int CNodo::GetIndY()
{
	return indY;
}

void CNodo::SetIndY(int indY)
{
	this->indY = indY;
}
