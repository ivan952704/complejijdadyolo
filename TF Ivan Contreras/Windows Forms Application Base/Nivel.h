
#include <stdio.h>

class CNivel
{
private:
	int CodigoNivel;
	int PuntosEnemigo;
	int FrecuenciaEnemigos;
	int CantidadObstaculos;
public:
	CNivel();
	~CNivel();

	void CargarNivelDeArchivo(FILE *f);
	void GuardarNivelEnArchivo(FILE *f);

	int GetCodigoNivel();
	void SetCodigoNivel(int codigo);
	int GetPuntosEnemigo();
	void SetPuntosEnemigo(int puntos);
	int GetFrecuenciaEnemigos();
	void SetFrecuenciaEnemigos(int frecuencia);
	int GetCantidadObstaculos();
	void SetCantidadObstaculos(int cantidad);
};
