
using namespace System::Drawing;

class CMisil
{
private:
	int x, y, dx, w, h, ind;
public:
	CMisil(int x, int y, int dir);
	~CMisil();

	void Pintar(Graphics ^g, Bitmap ^img);
	void Mover(Graphics ^g);

	int GetDX();
	void SetDX(int dx);
	int GetX();
	int GetY();
	int GetXW();
	int GetYH();
};

