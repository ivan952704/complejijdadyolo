#pragma once

#include "frmComando.h"

namespace WindowsFormsApplicationBase {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for frmInstrucciones
	/// </summary>
	public ref class frmInstrucciones : public System::Windows::Forms::Form
	{
	public:
		frmInstrucciones(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~frmInstrucciones()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pbxInstrucciones;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmInstrucciones::typeid));
			this->pbxInstrucciones = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxInstrucciones))->BeginInit();
			this->SuspendLayout();
			// 
			// pbxInstrucciones
			// 
			this->pbxInstrucciones->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pbxInstrucciones.Image")));
			this->pbxInstrucciones->Location = System::Drawing::Point(56, 94);
			this->pbxInstrucciones->Name = L"pbxInstrucciones";
			this->pbxInstrucciones->Size = System::Drawing::Size(100, 50);
			this->pbxInstrucciones->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pbxInstrucciones->TabIndex = 0;
			this->pbxInstrucciones->TabStop = false;
			this->pbxInstrucciones->Visible = false;
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button1->Location = System::Drawing::Point(637, 420);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(119, 42);
			this->button1->TabIndex = 8;
			this->button1->Text = L"Regresar al Menu";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &frmInstrucciones::button1_Click);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->Location = System::Drawing::Point(495, 420);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(119, 42);
			this->button2->TabIndex = 9;
			this->button2->Text = L"Comandos";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &frmInstrucciones::button2_Click);
			// 
			// frmInstrucciones
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(769, 476);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pbxInstrucciones);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(785, 515);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(785, 515);
			this->Name = L"frmInstrucciones";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"INSTRUCCIONES";
			this->Load += gcnew System::EventHandler(this, &frmInstrucciones::frmInstrucciones_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &frmInstrucciones::frmInstrucciones_Paint);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbxInstrucciones))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void frmInstrucciones_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		Graphics ^g = this->CreateGraphics();
		int gWidth = (int)g->VisibleClipBounds.Width;
		int gHeigth = (int)g->VisibleClipBounds.Height;
		g->DrawImage(pbxInstrucciones->Image, 0, 0, gWidth, gHeigth);
		delete g;
	}
	private: System::Void frmInstrucciones_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		frmComando ^f = gcnew frmComando();
		f->ShowDialog();
	}
};
}
