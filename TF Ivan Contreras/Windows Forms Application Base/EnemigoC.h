
class CEnemigoC : public CEnemigo
{
private:
	CMisil **Arreglo_Misiles;
	int NroMisiles;
	int cont_agregar;
public:
	CEnemigoC(int x, int y, int dir);
	~CEnemigoC();

	void Pintar(Graphics ^g, Bitmap ^img);
	void Mover(Graphics ^g);

	void Pintar_Balas(Graphics ^g, Bitmap ^img);
	void Mover_Balas(Graphics ^g);
	void Agregar_Bala();
	void Eliminar_Bala();

	int GetNumeroMisiles();
	CMisil *GetMisil(int indice);
};