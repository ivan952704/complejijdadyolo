
#include "Personaje.h"
#include "Nivel.h"
#include "Pila.h"
#include <vector>

using namespace std;

class CJuego
{
private:
	CPersonaje *objPersonaje;
	int puntos_ganados;
	CPila *movimientos;
	bool retroceso;
	int iteraciones;

	CEnemigo **ArregloEnemigos;
	int NroEnemigos;
	int AparicionEnemigos;
	int EnemigosEliminados;

	int NroNiveles;
	vector<CNivel*> VectorNiveles;
	int NivelActual;
	int NivelDesbloqueado;
public:
	CJuego();
	~CJuego();

	void Pintar_Personaje(Graphics ^g, Bitmap ^img);
	void Cambiar_Direccion_Personaje(Direccion dir);
	void Mover_Personaje(Graphics ^g);
	Direccion Get_Direccion_Personaje();
	void Personaje_Salta();
	void Personaje_Elimina_Enemigo(int *px, int *py);
	void Inicilizar_Salto_Personaje();
	int Get_IndiceX_Personaje();
	int Get_X_Personaje();
	int Get_Y_Personaje();
	bool Get_Saltando_Personaje();
	void SetPersonajeY(int y);
	int Get_Cantidad_Enemigos();
	Direccion Get_Direccion_Enemigo(int indice);
	bool Get_Retroceso();

	void Pintar_Enemigos(Graphics ^g, Bitmap ^imgA, Bitmap ^imgB, Bitmap ^imgC);
	void Mover_Enemigos(Graphics ^g);
	void Agregar_Enemigos();
	void Eliminar_Enemigos(Graphics ^g);
	bool ExisteColision(int indice, Direccion direccion);
	int GetXEnemigo(int indice);

	bool Enemigo_Elimina_Personaje();
	void Enemigo_Agrega_Misil();
	void Enemigo_Elimina_Misil();
	void Enemigo_Pinta_Misiles(Graphics ^g, Bitmap ^img);
	void Enemigo_Mueve_Misiles(Graphics ^g);
	bool Misil_Elimina_Personaje();

	void GuardarPosiciones();
	void RetrocesoTiempo();

	void Inicializar_Todo();
	void Siguiente_Nivel();
	void Crear_Niveles(FILE *f);
	void Cargar_Niveles(FILE *f);
	void Cargar_Partida(FILE *f);
	void Guardar_Partida(FILE *f);

	int GetPuntosGanados();
	void SetPuntosGanados(int puntos);
	int GetEnemigosEliminados();
	int GetNivelDesbloqueado();
	void SetNivelDesbloqueado(int nivel);
	int GetNivelActual();
	int GetNroNiveles();
	int GetIteraciones();
	void SetNivelActual(int nivel);
	void EliminaEnemigoPosicion(int pos);
}; 
