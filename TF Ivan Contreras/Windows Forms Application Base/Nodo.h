
class CNodo
{
private:
	CNodo *next;
	float x, y;
	int indX, indY;
public:
	CNodo();
	~CNodo();

	CNodo *GetNext();
	void SetNext(CNodo *next);
	float GetX();
	void SetX(float x);
	float GetY();
	void SetY(float y);
	int GetIndX();
	void SetIndX(int indX);
	int GetIndY();
	void SetIndY(int indY);
};
